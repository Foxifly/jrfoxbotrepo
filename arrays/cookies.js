module.exports = [
    'an almond cookie!',
    'a chocolate chip cookie! My favorite! :cookie:', "a chocolate chunk cookie ;3", "a box of Thin Mints!",
    'a cinnamon swirl cookie!', "some Samoa cookies.",
    'a christmas cookie! :santa:',
    'a butter cookie! That\'s awfully plain!', 'a gingerbread cookie! I hope they decorated it!',
    'a fortune cookie that says \'Keep your head held high\'', 'a fortune cookie that says \'Be great today!\'',
    'a fortune cookie that says \'You will come into great wealth!\'', 'a lemon flavored cookie! Hope it\'s not TOO sour!',
    'a mint cookie!', 'a molasses cookie!', 'a macaroon! Yummy!',
    'a cookie with nuts! Hope you are not allergic!',
    'a peanut butter cookie!',
    'a pumpkin cookie!',
    'a snickerdoodle!', 'a plain ole sugar cookie!',
    'a triple fudge cookie! OH SNAP! Give me some!',
    'an oreo cookie and a big glass of milk!'
];