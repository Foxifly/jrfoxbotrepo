module.exports = [
    "Wow. Try sharing that candy with other users.", "No candy for you!", "Sharing is caring.", "You can only give candy to other users, not yourself!", "Don't be rude, share!", "Sorry, I can't let you give candy to yourself.", "Trying to give yourself candy? Here's a carrot instead.", "Instead of giving yourself candy, maybe you should visit the gym!"
];