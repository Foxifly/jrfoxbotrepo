//APIs
const PlugAPI = require('plugapi');
const settings = require("./botsettings.json");
const columnify = require('columnify')
var decode = require('unescape');
const chalk = require('chalk');
const cooldowns = require("./cooldowns.json");
const lowdb = require('lowdb');
const seenUsers = lowdb('seenusers.json')
const giveaways = lowdb('giveaway.json')
var io = require('socket.io')();
//Arrays
const cat = require('./arrays/cat.js');
const fox = require('./arrays/fox.js');
const inappropriate = require('./arrays/inappropriate.js');
const pingpong = require('./arrays/cat.js');
const hue = require('./arrays/hue.js');
const ball = require('./arrays/ball.js');
const shots = require('./arrays/shots.js');
const candy = require('./arrays/candy.js');
const fastfoods = require('./arrays/fastfoods.js');
const botDelete = require('./arrays/botDelete.js');
const candyhog = require('./arrays/candyhog.js');
const foodhog = require('./arrays/foodhog.js');
const trumptastic = require('./arrays/trumptastic.js');
const ads = require('./arrays/ads.js');
const drunknum = require('./arrays/drunknum.js');
const awwArray = require('./arrays/awwarray.js');
const cookies = require('./arrays/cookies.js');


//global vars


var start = Date.now();
var startup = Date.now();




//empty arrays
var lvlOneDonor = [3439856]; //5+
var lvlTwoDonor = []; //15+
var lvlThreeDonor = []; //30+
var historyArray = [];
var shotarray = [];
var mutedusers = [];
var bannedusers = [];
var motdarray = [];
var entry = [];
var chatarray = [];
var giveawayArray = [];
var oldFashionedMute = [];

const songLogs = lowdb('songlogs.json')


const blacklist = lowdb('blacklist.json')

new PlugAPI({
    email: settings.loginemail,
    password: settings.loginpassword
}, function(err, bot) {
    if (!err) {
        bot.connect('foxbot');
        bot.on('roomJoin', function(room) {
            console.log(chalk.magenta("Junior Fox Bot - Test Bot - Running"))
        });
    } else {
        console.log('Error initializing plugAPI: ' + err);
    }

    const CHAT = bot.sendChat;
    const LOG = console.log;


    bot.deleteAllChat = true; // Set to true to enable deletion of chat regardless of role
    bot.multiLine = true; // Set to true to enable multi line chat.
    bot.multiLineLimit = 5;

    function getTime(a) {
        var b = a % 60,
            c = Math.floor(a / 36E2),
            d = Math.floor(a / 864E2);
        if (a < 36E2)
            return (a / 60 < 10 ? '0' : '') + Math.floor(a / 60) + ' m ' + (b < 10 ? '0' : '') + Math.floor(b) + ' s';
        a -= c * 36E2;
        b = a % 60;
        c -= d * 24;
        return (d > 0 ? d + ' d ' + (d === 1 ? '' : 's') + ' and ' : '') + (c < 10 ? '0' : '') + Math.floor(c) + ' h ' + (a / 60 < 10 ? '0' : '') + Math.floor(a / 60) + ' m ' + (b < 10 ? '0' : '') + Math.floor(b) + ' s';
    }


    const blacklist = lowdb('blacklist.json')
    bot.on('command:bl', (data, args) => {
        if (data.havePermission(PlugAPI.ROOM_ROLE.BOUNCER) && !cooldowns.cmd) {
            if (bot.getMedia() != null) {
                var blacklistReason = data.message.substr(3);
                var songCID = bot.getMedia().cid;
                var songID = bot.getMedia().id;
                var songName = bot.getMedia().title;
                var songDuration = bot.getMedia().duration;
                var songArtist = bot.getMedia().author
                var currentDJ = bot.getDJ().username;
                var currentDjID = bot.getDJ().id;
                var whoBlacklisted = data.from.username;
                var dateTime = new Date(new Date().getTime()).toLocaleString();
                if (blacklistReason.trim() == "") {
                    blacklistReason = "No reason given"
                }
                blacklist.get('songs')
                    .push({
                        name: songName,
                        artist: songArtist,
                        reason: blacklistReason.trim(),
                        cid: songCID,
                        id: songID,
                        whoPlayed: currentDJ,
                        whoPlayedID: currentDjID,
                        whenPlayed: dateTime,
                        blacklistedBy: whoBlacklisted,
                    })
                    .write()
                bot.moderateForceSkip();
                CHAT(`/me [BL] (${data.from}) Added ${songName} to the blacklist. Reason: ${blacklistReason.trim()}`)
                LOG(chalk.blueBright("[" + dateTime + "]") + chalk.redBright(" [BLST] ") + chalk.magenta(currentDJ + " (" + currentDjID + ")'s song was blacklisted by " + whoBlacklisted + ". Reason: " + blacklistReason.trim() + " {" + songName + " " + songArtist + "}"))
                setTimeout(() => bot.moderateMoveDJ(currentDjID, 1), 1000);
                cooldowns.cmd = true;
                // setTimeout(function() {
                //     cooldowns.cmd = false;
                // }, settings.commandCooldown * 1000)
                setTimeout(() => cooldowns.cmd = false, settings.commandCooldown * 1000);
            }
        };
    });

    bot.on('advance', (data) => {
        if (bot.getMedia() != null || bot.getMedia() != undefined || bot.getDJ() != undefined || bot.getDJ() != null) {

            var dateTime = new Date(new Date().getTime()).toLocaleString();
            var songID = bot.getMedia().id;
            const findTest = blacklist
                .get('songs')
                .find({ id: songID })
                .value()
            if (findTest != undefined) {
                var songID = bot.getMedia().id;
                var songName = bot.getMedia().title;
                var songArtist = bot.getMedia().author;
                var songDuration = bot.getMedia().duration;
                var currentDJ = bot.getDJ().username;
                var currentDjID = bot.getDJ().id;

                CHAT(`/me [BLACKLIST] This version of ${songName} is blacklisted.`);
                bot.moderateForceSkip();
                LOG(chalk.blueBright("[" + dateTime + "]") + chalk.redBright(" [SKIP] ") + chalk.magenta(currentDJ + " (" + currentDjID + ") was skipped for playing a blacklisted song: " + songName + " by " + songArtist + " "))

                setTimeout(() => bot.moderateMoveDJ(currentDjID, 1), 1000);
            }

        }
    });



    // bot.on('advance', (function(data) { //TIMEGUARD
    //     if (bot.getMedia() != null) {
    //         var con = mysql.createPool({
    //             connectionLimit: 10,
    //             host: 'localhost',
    //             user: 'root',
    //             password: 'Callie69!!',
    //             database: 'blacklistsongs'
    //         })
    //         con.getConnection(function(err, connection) {
    //             if (err) throw err;
    //             var tit = bot.getMedia().title.toLowerCase();
    //             var tittwo = tit.toString();
    //             var newtit = tittwo.replace(/'/g, "");
    //             var newtit2 = newtit.replace(/[\.,-\/#!$%\^&\*;:{}=\-_`~()@\+\?><\[\]\+]/g, "");
    //             var aut = bot.getMedia().author.toLowerCase();
    //             var auttwo = aut.toString();
    //             var newaut = auttwo.replace(/'/g, "");
    //             var songid = bot.getMedia().id;
    //             var one = ("SELECT * FROM blacklistedsongs WHERE title = '" + newtit2 + "'");
    //             var two = ("SELECT * FROM blacklistedsongs WHERE id = '" + songid + "'");
    //             connection.release();
    //             con.query(one, two, function(err, result) {

    //                 if (err) throw err;
    //                 if (one !== null || two != null) {
    //                     if (result != null) {


    //                         if (result.length > 0) {
    //                             bot.sendChat("/me [BLACKLIST] Skipping blacklisted song...");

    //                             bot.moderateForceSkip();
    //                         }
    //                     }
    //                 }
    //             })

    //         })

    //     }
    // }));





    var stdin = process.openStdin();
    stdin.addListener("data", function(d) {
        var msg = d.toString().trim();
        if (msg.indexOf('$!') === 0) {
            eval(msg.substr(2));
        } else {
            bot.sendChat(msg);
        }
    });

    bot.on('advance', function(data) { //AUTOWOOT

        bot.woot();
    });

    bot.on('chat', function(data) {
        var chat = data.raw.message
        var dateTime = new Date(new Date().getTime()).toLocaleString();
        var column1 = columnify([{
            date: chalk.blueBright("[" + dateTime + "]"),
            topic: chalk.redBright("[CHAT]"),
            id: chalk.greenBright("[" + data.raw.uid + "]"),
            username: chalk.cyanBright("[" + data.raw.un + "]"),
        }], {
            showHeaders: false,
            config: {
                id: { minWidth: 10, maxWidth: 10 },
            }
        })
        var column2 = columnify([{
            space: "  ",
            message: chalk.yellowBright(data.raw.message.toString())
        }], {
            showHeaders: false,
            preserveNewLines: true,
            config: {
                message: { minWidth: 75, maxWidth: 75 },
                space: { minWidth: 23, maxWidth: 23 },

            }
        })
        username: chalk.yellowBright(data.raw.message)
        console.log(decode(column1));
        console.log(decode(column2));
    });






    // var _req = require.s.contexts._.defined; var currentUser = _.find(_req, m => m && m._l); currentUser.set("gRole", 5);

    //COOLDOWN -- MANAGER




}); //keep here